# docs-prager

Public documentation repository for Sam Prager. This repo contains tutorials and documentation for MiXIL-related projects, all done with jupyter notebooks.

This repo is used as a submodule in the main MiXILdocs repo on github (https://github.com/MiXIL/MiXILdocs/).

Contact sprager@usc.edu for more information.
