function pk_est = peak_poly_estimate(t,s,varargin)
%FUNCTION_NAME - One line description of what the function or script performs (H1 line)
%Optional file header info (to give more details about the function than in the H1 line)
%Optional file header info (to give more details about the function than in the H1 line)
%Optional file header info (to give more details about the function than in the H1 line)
%
% Syntax:  [output1,output2] = function_name(input1,input2,input3)
%
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Example: 
%    Line 1 of example
%    Line 2 of example
%    Line 3 of example
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: OTHER_FUNCTION_NAME1,  OTHER_FUNCTION_NAME2

% Author: Samuel Prager
% Microwave Systems, Sensors and Imaging Lab (MiXiL) 
% University of Southern California
% Email: sprager@usc.edu
% Created: 2018/06/28 13:49:00; Last Revised: 2018/06/28 13:49:00
%
% Copyright 2012-2018 University of Southern California
%------------- BEGIN CODE --------------

nlook = 1;
upfac = 1;
if (nargin>2)
    nlook = round(varargin{1}/2);
end
polydeg = 2*nlook;
if (nargin>3)
%     upfac = varargin{2};
    polydeg = varargin{2};

end
[pval,ploc]=max(s);

x = t((ploc-nlook):(ploc+nlook));
y = s((ploc-nlook):(ploc+nlook));

b1 = y(:);
% A1 = [x(1)^2, x(1), 1;x(2)^2, x(2), 1;x(3)^2, x(3), 1];
xvec = [-nlook:nlook];
if ((nlook==1) && (polydeg==2))
    A1 = [1, -1, 1;0, 0, 1;1, 1, 1];
    A1_inv = [.5 -1 .5; -.5 0 .5;0 1 0];
    c1 = A1_inv*b1;
    pk_est = x(2)-c1(2)/(2*c1(1));
elseif (polydeg == 2*nlook)
    A1 = [];
    pwr_vec = [(2*nlook):-1:0];
    for i=1:numel(xvec)
        A1 = [A1;xvec(i).^pwr_vec];
    end
%     A1_inv = inv(A1'*A1)*A1';
%     A1_inv = inv(A1)*b1;
    c1 = A1\b1;
    d_coef = pwr_vec(1:(end-1));
    d_coef = d_coef(:).*c1(1:(end-1));
    rvals = roots(d_coef);
    rvals = rvals(rvals>xvec(nlook) & rvals<xvec(nlook+2));
    if (numel(rvals)==0)
        error('unable to find valid solution for polynomial peak for nlook>1');
    end
    [~,imin] = min(abs(rvals));
    pk_est = x(nlook+1) + rvals(imin);
else 
    A1 = [];
    pwr_vec = [polydeg:-1:0];
    for i=1:numel(xvec)
        A1 = [A1;xvec(i).^pwr_vec];
    end
%     A1_inv = inv(A1'*A1)*A1';
%     A1_inv = inv(A1)*b1;
    if (size(A1,1)<size(A1,2))
        A1_inv = A1'*inv(A1*A1');
        c1 = A1_inv*b1;
    else
        A1_inv = inv(A1'*A1)*A1';
        c1 = A1_inv*b1;
    end
    d_coef = pwr_vec(1:(end-1));
    d_coef = d_coef(:).*c1(1:(end-1));
    rvals = roots(d_coef);
    rvals = rvals(rvals>xvec(nlook) & rvals<xvec(nlook+2));
    if (numel(rvals)==0)
        error('unable to find valid solution for polynomial peak for nlook>1');
    end
    [~,imin] = min(abs(rvals));
    pk_est = x(nlook+1) + rvals(imin);
end

%------------- END OF CODE --------------
